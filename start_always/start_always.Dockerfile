FROM registry.cn-hangzhou.aliyuncs.com/whyun/base:golang-1.17.2 AS build-stage
COPY delay_exit.go /opt
WORKDIR /opt
RUN go build -o delay-exit delay_exit.go

FROM centos:7
# 使用东八区时区
RUN ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
COPY --from=build-stage /opt/delay-exit /opt/delay-exit
COPY start.sh /start.sh
ENTRYPOINT ["/start.sh"]