package main

import (
	"fmt"
	"os"
	"strconv"
	"time"
)

func main() {
	fmt.Println("begin", time.Now().Format("2006-01-02 15:04:05"))
	seconds := 11
	if len (os.Args) == 2 {
		secondsFromCli, _ := strconv.Atoi( os.Args[1] )
		if secondsFromCli > 0 {
			seconds = secondsFromCli
		}
	}
	time.Sleep(time.Duration(seconds) * time.Second)
	fmt.Println("exit", time.Now().Format("2006-01-02 15:04:05"))
	os.Exit(1)
}